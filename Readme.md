# TTS - Power Grid USA
TTS - Power Grid USA is a LUA script written for [Tabletop Simulator](https://www.tabletopsimulator.com). This repository contains source code for the project as well as a save file that may be loaded into TTS. This save contains scripting zones not found in the version released on Steam Workshop.

## Getting Started
Once you have everything set up, you can get started working on the project via the following guidelines:
1. Subscribe to the [Power Grid USA mod](https://steamcommunity.com/sharedfiles/filedetails/?id=2039197975). You must be friend with Kellbot to access this.
2. Start Tabletop Simulator.
3. Open up Atom (with the TTS plugin already installed).
4. Create a new server.
5. Load the workshop mod (Games > Workshop > Codenames).
6. Open up Atom. The script should already be loaded in the editor. If not, use the `CTRL` + `SHIFT` + `L` key combination to get LUA scripts from the game.
7. Clone the repository and copy the contents of the files from the repository into their respectively named files on your local instance (usually something like `Temp/Tabletop Simulator/Tabletop Simulator Lua Scripts`.
8. Use `CTRL` + `SHIFT` + `S` to load the files into the game
9. Make / test / save your changes
10. Copy your changes into the directory containing the repository
11. Check in your changes
