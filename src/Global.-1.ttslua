selectedColor = "#00CCCC"

phaseTrackerGuid = '5cba75'
fiftiesGuid = "9a7185"
tensGuid = "aebb29"
fivesGuid = "db7e33"
onesGuid = "8abd5c"
powerPlantCardsGuid = "65b2c8"
stepThreeGuid = "e36b67"


-- These are the target zones on the board. There is one for each price box of resources.
resourceZones = {
  {guid = '8e2a3f', cost = 1, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '043018', cost = 2, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = 'd8b033', cost = 3, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = 'c379a9', cost = 4, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '0bb904', cost = 5, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '840001', cost = 6, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '075955', cost = 7, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '3ebcf9', cost = 8, maxCoal = 3, maxOil = 3, maxTrash = 3, maxUranium = 1},
  {guid = '0588fa', cost = 10, maxCoal = 0, maxOil = 0, maxTrash = 0, maxUranium = 1},
  {guid = '8835c9', cost = 12, maxCoal = 0, maxOil = 0, maxTrash = 0, maxUranium = 1},
  {guid = '515c0a', cost = 14, maxCoal = 0, maxOil = 0, maxTrash = 0, maxUranium = 1},
  {guid = '94aa98', cost = 16, maxCoal = 0, maxOil = 0, maxTrash = 0, maxUranium = 1},
}

--resource bags
CoalGuid = 'cce2d0'
OilGuid = '39d679'
TrashGuid = '02b728'
UraniumGuid = 'b1a8fd'


-- Refersh quantities vary by the number of players and phase of game
refreshQuantities = {
  --number of players
  [2] = {
    --phase 1
    [1] = { Coal = 3, Oil = 2, Trash = 1, Uranium = 1},
    --phase 2
    [2] = { Coal = 4, Oil = 2, Trash = 2, Uranium = 1},
    --phase 3
    [3] = { Coal = 3, Oil = 4, Trash = 3, Uranium = 1},
  },
  [3] = {
    [1] = { Coal = 4, Oil = 2, Trash = 1, Uranium = 1},
    [2] = { Coal = 5, Oil = 3, Trash = 2, Uranium = 1},
    [3] = { Coal = 3, Oil = 4, Trash = 3, Uranium = 1},
  },
  [4] = {
    [1] = { Coal = 5, Oil = 3, Trash = 2, Uranium = 1},
    [2] = { Coal = 6, Oil = 4, Trash = 3, Uranium = 2},
    [3] = { Coal = 4, Oil = 5, Trash = 4, Uranium = 2},
  },
  [5] = {
    [1] = { Coal = 5, Oil = 4, Trash = 3, Uranium = 2},
    [2] = { Coal = 7, Oil = 5, Trash = 3, Uranium = 3},
    [3] = { Coal = 5, Oil = 6, Trash = 5, Uranium = 2},
  },
  [6] = {
    [1] = { Coal = 7, Oil = 5, Trash = 3, Uranium = 2},
    [2] = { Coal = 9, Oil = 6, Trash = 5, Uranium = 3},
    [3] = { Coal = 6, Oil = 7, Trash = 6, Uranium = 3},
  },
}

-- points for resources to snap to
resourceZoneSnaps = {
  ["Coal"] = { {-0.35, 0, 0.35}, {0, 0, 0.35}, {0.3, 0, 0.35} },
  ["Oil"] = { {-0.4, 0, 0}, {-0.125,  0, 0}, {0.125, 0,    0} },
  ["Uranium"] = { {0.38, 0, 0} },
  ["Trash"] = { {-0.35, 0,  -0.35}, {0, 0,  -0.35}, {0.3, 0,  -0.35} }
}

--[[ The OnLoad function. This is called after everything in the game save finishes loading.
Most of your script code goes here. --]]
function onload()

  UI.show("flyingButtons")

  phaseTracker = getObjectFromGUID(phaseTrackerGuid)
  phaseCount = phaseTracker.call('getPhase')
end

--[[ The Update function. This is called once per frame. --]]
function update ()

end

function onNextRound()
  updateResources()
end

-- IDs of all the player buttons in the UI. Surely there's a way to get this?
playerButtons = {'playerCount1', 'playerCount2','playerCount3','playerCount4','playerCount5','playerCount6'}

function onSettings()
  for __, id in ipairs(playerButtons) do
    UI.setAttribute(id, "color", '#FFFFFF')
  end
  -- color the button with the current number of Players
  if (playerCount) then UI.setAttribute('playerCount'..playerCount, 'color', "#00CCCC") end

  --it makes me so angry that this returns the string true/false instead of a boolean
  if UI.getAttribute('settingsPanel','active') == 'true' then
    UI.hide('settingsPanel')

  else
    UI.show('settingsPanel')
  end
end

--[[This is where we set up the board for the beginning of the game ]]
function onSetup()
  -- CHeck for manual player count. If none, count the number of seated players and use that
  if not (playerCount) then
    players = getSeatedPlayers()
    playerCount = #players
  end

  --reset the phase tracker to 1
  phaseTracker = getObjectFromGUID(phaseTrackerGuid)
  phaseTracker.call('reset')

  -- deal money to Players
  getObjectFromGUID(tensGuid).deal(3)
  getObjectFromGUID(fivesGuid).deal(3)
  getObjectFromGUID(onesGuid).deal(5)

  -- Shuffle the power plant cards
  ppCards = getObjectFromGUID(powerPlantCardsGuid)
  ppCards.shuffle()
  stepThree = getObjectFromGUID(stepThreeGuid)
  stepThree.flip()

  --[[ place all the power plant cards on top of the step 3 card
  and await a day when you can just place a card on the bottom of a deck
  ]]--
  ppCards.setPosition(stepThree.getPosition())

  UI.hide('setupButton')
  UI.show('resourcesButton')

end

function onPlayers(player, buttonNumber)

  for __, id in ipairs(playerButtons) do
    UI.setAttribute(id, "color", '#FFFFFF')
  end
  UI.setAttribute('playerCount'..buttonNumber, "color", selectedColor)

  playerCount = tonumber(buttonNumber)
end


-- Placing N of a given resource should be abstracted out of this
function updateResources()

  phaseCount = phaseTracker.call('getPhase')


--  populate the resourceZones array with how many of each item are in the given zone
  for __, zone in ipairs(resourceZones) do
    local resources = countZoneResources(zone['guid'])

    zone['Coal'] = resources['Coal']
    zone['Oil'] = resources['Oil']
    zone['Trash'] = resources['Garbage']
    zone['Uranium'] = resources['Uranium']
  end

  -- some empty counters for counting how many of each resource are already out
  Oil = 0
  Coal = 0
  Trash = 0
  Uranium = 0

 -- get the current number of resources to add
  local newResources =  table.clone(refreshQuantities[playerCount][phaseCount])


-- loop through zones backwards to start with the highest cost zone
  for i = #resourceZones, 1, -1 do
      zoneData = resourceZones[i]


      --[[now we do some stuff with the _G global scope so I don't have to write everything out four times.
       Not sure this is a good idea. It's definitely harder to read. ]]--

      resourceTypes = {"Coal","Oil","Trash","Uranium"}
      for __, resourceType in ipairs(resourceTypes) do
        --increment our counter. _G["oil"] references the oil variable, etc.
        _G[resourceType]  =_G[resourceType] + zoneData[resourceType]
      end

      --check to see if the current zone can support more of a given resource
      for __, resourceType in ipairs(resourceTypes) do
        -- look at maxCoal, maxOil, etc

        if newResources[resourceType] > 0 then
          --the if statements are separate for debugging
          if (zoneData["max"..resourceType] > zoneData[resourceType]) then
            local emptySpaces = zoneData["max"..resourceType] - zoneData[resourceType]
            -- calculate how many we're placing (the lesser of either the available spaces or the total number to place)
            local  toPlace  = math.min(emptySpaces, newResources[resourceType])

            local zone = getObjectFromGUID(zoneData["guid"])
            local resourceBag = getObjectFromGUID(_G[resourceType..'Guid'])


            if resourceBag.getQuantity() > 0 then
              -- now we actually place the resources
              for i = 1, toPlace, 1 do
                --take one of the resource in question
                resource = resourceBag.takeObject()

                -- the first 8 zones have multiple snap points, the others just go to the center
                if (zoneData['cost'] <= 8 ) then
                  positionIndex = zoneData[resourceType] + 1
                  targetPos = zone.positionToWorld(resourceZoneSnaps[resourceType][positionIndex])
                else
                  targetPos = zone.getPosition()
                end
                -- move it to the zone.
                resource.setPosition(targetPos)
                -- increment the zoneData
                zoneData[resourceType] = zoneData[resourceType] + 1
              end

              -- reduce the number of new resources we need by however many we put out
              newResources[resourceType] = newResources[resourceType] - toPlace

            else   -- the bag is empty so we're not going to put any more out
              newResources[resourceType] = 0
            end

          end
        end
      end

    end




end

function countZoneResources(guid)
  currentZone = getObjectFromGUID(guid)
  resources = currentZone.getObjects()
  local resourceCounts = {
    ['Oil'] = 0,
    ['Coal'] = 0,
    ['Garbage'] = 0,
    ['Uranium'] = 0,
  }

  for __, resource in ipairs(resources) do
    resourceCounts[resource.getName()] = resourceCounts[resource.getName()] + 1
  end

  return resourceCounts

end

function table.clone(orig)
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
      copy = {}
      for orig_key, orig_value in pairs(orig) do
          copy[orig_key] = orig_value
      end
  else -- number, string, boolean, etc
      copy = orig
  end
  return copy
end
